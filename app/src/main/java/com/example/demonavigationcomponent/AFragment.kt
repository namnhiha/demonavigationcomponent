package com.example.demonavigationcomponent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_a.*
import kotlinx.android.synthetic.main.fragment_a.view.*

class AFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_a, container, false)
        view.buttonStartB.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_AFragment_to_BFragment)
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        buttonStartB.setOnClickListener {
            val action=AFragmentDirections.actionAFragmentToBFragment()
            it.findNavController().navigate(action)
        }
    }
}
