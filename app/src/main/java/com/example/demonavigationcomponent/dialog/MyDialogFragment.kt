package com.example.demonavigationcomponent.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.demonavigationcomponent.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MyDialogFragment:BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.my_dialog,container,false)
    }
}
